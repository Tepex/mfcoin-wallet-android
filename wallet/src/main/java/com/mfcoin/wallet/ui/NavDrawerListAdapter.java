package com.mfcoin.wallet.ui;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.mfcoin.wallet.R;
import com.mfcoin.wallet.ui.widget.NavDrawerItemView;

import java.util.ArrayList;
import java.util.List;

/**
 * @author John L. Jegutanis
 */
public class NavDrawerListAdapter extends BaseAdapter {
    private final Context context;
    private final LayoutInflater inflater;
    private List<NavDrawerItem> items = new ArrayList<>();

    public NavDrawerListAdapter(final Context context, List<NavDrawerItem> items) {
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.items = items;
    }

    public void setItems(List<NavDrawerItem> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        return items.get(position).getType().ordinal();
    }

    @Override
    public int getViewTypeCount() {
        return NavDrawerItem.Type.values().length;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public NavDrawerItem getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View row, ViewGroup parent) {
        NavDrawerItem item = getItem(position);

        if (row == null) {
            switch (item.getType()) {
                case ITEM_SEPARATOR:
                    row = inflater.inflate(R.layout.nav_drawer_separator, null);
                    break;
                case ITEM_SECTION_TITLE:
                    row = inflater.inflate(R.layout.nav_drawer_section_title, null);
                    break;
                case ITEM_COIN:
                case ITEM_OVERVIEW:
                case ITEM_TRADE:
                case ITEM_CHAT:
                case ITEM_VNN:
                    row = new NavDrawerItemView(context);
                    break;
                default:
                    throw new RuntimeException("Unknown type: " + item.getType());
            }
        }

        if(isSeparator(item.getType())) setNotClickable(row);

        switch(item.getType())
        {
            case ITEM_SECTION_TITLE:
                if(row instanceof TextView) ((TextView)row).setText(item.getTitle());
                break;
            case ITEM_COIN:
            case ITEM_OVERVIEW:
            case ITEM_TRADE:
            case ITEM_CHAT:
            case ITEM_VNN:
                ((NavDrawerItemView)row).setData(item.getTitle(), item.getIconRes());
                break;
        }

        return row;
    }

    private boolean isSeparator(NavDrawerItem.Type itemType) {
        return itemType == NavDrawerItem.Type.ITEM_SEPARATOR || 
        	itemType == NavDrawerItem.Type.ITEM_SECTION_TITLE;
    }

    @Override
    public boolean isEnabled(int position) {
        return !isSeparator(getItem(position).getType());
    }

    private void setNotClickable(View view) {
        view.setClickable(false);
        view.setFocusable(false);
        view.setContentDescription("");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            view.setImportantForAccessibility(View.IMPORTANT_FOR_ACCESSIBILITY_NO);
        }
    }
}