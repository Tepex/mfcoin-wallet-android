package com.mfcoin.wallet.ui;

import android.support.annotation.DrawableRes;

/**
 * @author John L. Jegutanis
 */
public class NavDrawerItemImpl implements NavDrawerItem 
{
	public NavDrawerItemImpl(Type itemType, String title)
	{
		this(itemType, title, 0);
	}
	
	public NavDrawerItemImpl(Type itemType, String title, @DrawableRes int iconRes)
	{
		this(itemType, title, iconRes, null);
	}
	
	public NavDrawerItemImpl(Type itemType, String title, @DrawableRes int iconRes, String accountId)
	{
		this.itemType = itemType;
		this.title = title;
		this.iconRes = iconRes;
		this.accountId = accountId;
	}
	
	@Override
	public Type getType()
	{
		return itemType;
	}
	
	@Override
	public String getTitle()
	{
		return title;
	}
	
	@Override
	@DrawableRes
	public int getIconRes()
	{
		return iconRes;
	}
	
	@Override
	public String getAccountId()
	{
		return accountId;
	}

	protected Type itemType;
	protected String title;
	@DrawableRes
	protected int iconRes;
	protected String accountId;
}
