package com.mfcoin.wallet.ui;

import android.support.annotation.DrawableRes;

public interface NavDrawerItem 
{
	Type getType();
	String getTitle();
	@DrawableRes
	int getIconRes();
	String getAccountId();
	
	public static enum Type
	{
		ITEM_SECTION_TITLE,
		ITEM_SEPARATOR, // FIXME, the separator does not work
		ITEM_COIN,
		ITEM_TRADE,
		ITEM_OVERVIEW,
		ITEM_CHAT,
		ITEM_VNN
	}
}
