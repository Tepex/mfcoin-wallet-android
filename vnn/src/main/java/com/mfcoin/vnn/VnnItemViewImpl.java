package com.mfcoin.vnn;

import android.content.Context;
import android.support.annotation.DrawableRes;

public class VnnItemViewImpl implements VnnItemView
{
    public VnnItemViewImpl(Context context)
    {
    	mainTitle = context.getString(R.string.navigation_drawer_vnn);
    	waitingTitle = context.getString(R.string.navigation_drawer_vnn_waiting);
    	connectedRes = R.drawable.ic_lock;
    	disconnectedRes = R.drawable.ic_unlock;
    	setState(State.DISCONNECTED);
    }
    
    @Override
    public void setState(State state)
    {
    	this.state = state;
    	switch(state)
    	{
    		case DISCONNECTED:
    			title = mainTitle;
    			iconRes = disconnectedRes;
    			break;
    		case CONNECTED:
    			title = mainTitle;
    			iconRes = connectedRes;
    			break;
    		case WAITING:
    			title = waitingTitle;
    			break;
    	}
    }
    
    @Override
    public State getState()
    {
    	return state;
    }
    
	private State state;
	private String title;
	private String mainTitle;
	private String waitingTitle;
	@DrawableRes
	private int iconRes;
	@DrawableRes
	private int disconnectedRes;
	@DrawableRes
	private int connectedRes;
}
