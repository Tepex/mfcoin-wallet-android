package com.mfcoin.vnn;

public interface VnnItemView
{
	void setState(State state);
	State getState();
	
	public static enum State
	{
		DISCONNECTED,
		CONNECTED,
		WAITING
	}
}
